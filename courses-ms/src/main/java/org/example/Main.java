package org.example;

import lombok.RequiredArgsConstructor;
import org.example.domain.StudentEntity;
import org.example.repo.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Optional;

//CommandLineRunner is when application finish to run method will run
@SpringBootApplication
@RequiredArgsConstructor
public class Main implements CommandLineRunner {

    private final StudentRepository studentRepository;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        StudentEntity studentEntity =
                StudentEntity
                        .builder()
                        .firstName("Fazil")
                        .lastName("Javadov")
                        .build();
    studentRepository.save(studentEntity);
    final Optional<StudentEntity> studentRepositoryById = studentRepository.findById(1L);
//    if (studentRepositoryById.isPresent()){
//        System.out.println(studentRepositoryById.get());
//    }
        studentRepositoryById.ifPresent(System.out::println); // this is function programming


    }
}