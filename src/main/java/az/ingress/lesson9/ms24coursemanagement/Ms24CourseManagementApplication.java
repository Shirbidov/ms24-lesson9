package az.ingress.lesson9.ms24coursemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms24CourseManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ms24CourseManagementApplication.class, args);
	}

}
